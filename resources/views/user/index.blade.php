@extends('layouts.app')
@section('title','Lista de Usuarios')
@section('content')
<style type="text/css">
    .pagination li{
        display: inline-block;
        padding-left: 3px;
        padding-right: 3px;
    }
    a{
        text-decoration: none;
        color: blue;
    }
</style>
<h1>Lista de usuarios</h1>
<a href="/users/create">nuevo</a>
<ul>
    @forelse ($users as $user)
    <li>Nombre: {{$user->name }} <br>Email: {{ $user->email}}
        <a href="/users/{{ $user->id }}/edit">editar</a>
        <form method="post" action="/users/{{ $user->id}}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <input type="submit" value="borrar">
        </form>
    </li>

    @empty
    <p>No hay usuarios</p>
    @endforelse
    </ul>
    {{ $users->render() }}
    @endsection
