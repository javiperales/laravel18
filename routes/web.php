<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('show/{id}', 'UserController@show')->name('usuarios');
// Route::get('users', 'UserController@index')->name('usuarios');
//hay que ponerlos por orden
//Ruta de tipo resource equivale a las 7 rutas
Route::get('users/especial', 'UserController@especial');
Route::resource('users','UserController');
//Las dos funcionan igual
// Route::resource('/users','UserController');
// });
// Route::get('users/{id}', function ($id) {
//     return "Dealle de usuario $id";
// });
// Route::get('users/{id}', function ($id) {
//     return "Dealle de usuario $id";
// });
// Route::get('users/{id}/{user}', function ($id,$user) {
//     return "Dealle de usuario $id con nombre $user";
// });
// Route::get('users/{id}/{$user?}', function ($id,$user=null) {
//     if ($user) {
//         return "Dealle de usuario $id con nombre $user";
//     } else {
//         return "Dealle de usuario $id y es Anonimo";
//     }
// });



/*fichero de entrada son los controladores.
De la ruta al controlador*/
